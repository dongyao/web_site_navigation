<?php

namespace app\index\controller;

use think\Controller;
use think\Request;

class Base extends Controller
{
    protected function initialize()
    {
        $sys_info = db('system')->find();
         // 友情链接
         $friend = db('friend')->order('l_sort desc')->limit(8)->select();
         $this->assign('friend',$friend);
        $this->assign('sys_info',$sys_info);
    }

}
